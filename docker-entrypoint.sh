#!/bin/bash
set -e

if [ ! -z "${NEWRELIC_LICENSE}" ]; then
	echo "NewRelic: enabled"
	sed -i "s/newrelic.license = .*/newrelic.license = "${NEWRELIC_LICENSE}"/g" /usr/local/etc/php/conf.d/newrelic.ini 

	echo "Setting NewRelic license: ${NEWRELIC_LICENSE}"
	echo "Setting NewRelic appname: ${NEWRELIC_APPNAME}"
	echo "Restting NewRelic to back agent mode and enabling"
	sed -i "s/newrelic.daemon.dont_launch = 3/newrelic.daemon.dont_launch = 0/g" /usr/local/etc/php/conf.d/newrelic.ini
	sed -i "s/;newrelic.enabled = true/newrelic.enabled = true/g" /usr/local/etc/php/conf.d/newrelic.ini 
	sed -i "s/newrelic.appname = .*/newrelic.appname = \"${NEWRELIC_APPNAME}\"/g" /usr/local/etc/php/conf.d/newrelic.ini

else
	echo "NewRelic: disabled"
fi
# if command starts with an option, prepend php-fpm
if [ "${1:0:1}" = '-' ]; then
	set -- php-fpm "$@"
fi
# if command is empty run the default docker CMD from the Dockerfile 
# or whatever you specified on the command line like php -a
exec "$@"