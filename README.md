# PHP-FPM Docker Image

This is a [PHP-FPM](http://php-fpm.org/) Docker image from the official docker php [image](https://github.com/docker-library/php) with additional extensions installed via [docker-php-ext-install](https://github.com/docker-library/php/blob/master/docker-php-ext-install) from the official image. 

Extensions not available by default are downloaded manually and put into the proper extension directory as per the official image at _/usr/src/php/ext/_ and then enabled via [docker-php-ext-install](https://github.com/docker-library/php/blob/master/docker-php-ext-install).

### Additional extension enabled/installed
- mbstring
- memcached
- opcache
- pdo_mysql
- redis
- zip

### Usage
To use this image you can define one or more pool definitions by mounting a volume to 
```/usr/local/etc/php/fpm/pool.d/``` .

This image also uses it's own _php.ini_ . You can override it by mounting ```-v``` or using ```COPY``` in a _Dockerfile_ to the container directory _/usr/local/etc/php/_ .

#### use a custom php.ini

```sh
docker run -it --rm -v "$PWD"/conf/php.ini:/usr/local/etc/php pmcjury/docker-php-fpm```
```

#### mount the current dir and a php-fpm pool configuration

```sh
docker run -it --rm --name my-fpm-php-app -v "$PWD":/var/www/html -v "$PWD"/conf/pool.d:/usr/local/etc/php/fpm/pool.d pmcjury/docker-php-fpm
```

# Author

- Author:: Patrick McJury(<pmcjury@yahoo.com>)